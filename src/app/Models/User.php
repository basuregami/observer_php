<?php
namespace App\Models;

use App\Observer\BaseSubject;
use App\Vendor\Database\BaseModel;

Class User extends BaseModel
{

  protected $fillable = [
    'name',
    'age'
  ];


}
