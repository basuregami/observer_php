<?php
namespace App\Observer;

use App\Observer\AbstractObserver;

class BaseObserver extends AbstractObserver
{

  public function update(AbstractSubject $subject) {
    writeln('*IN PATTERN OBSERVER - NEW PATTERN GOSSIP ALERT*');
    writeln(' new favorite patterns: '.$subject->getFavorites());
    writeln('*IN PATTERN OBSERVER - PATTERN GOSSIP ALERT OVER*');
  }

}
