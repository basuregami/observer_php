<?php
namespace App\Observer;

use App\Observer\AbstractSubject;

class UserObserver
{
  public function create()
  {
    return "hello from user Observer";
  }
}
