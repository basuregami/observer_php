<?php
 namespace App\Providers;

use App\Models\User;
use App\Observer\UserObserver;

class ObserverServiceProvider
 {

   public function boot()
   {
     User::observe(UserObserver::class);
   }
 }
