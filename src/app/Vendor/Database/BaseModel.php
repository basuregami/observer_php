<?php

 namespace App\Vendor\Database;

 use App\Vendor\Database\Support\QueryBuilder;
 use App\Vendor\Database\Support\Traits\HasEventsTrait;
 use App\Vendor\Database\Support\Traits\ForwardsCallsTrait;

 abstract class BaseModel
 {
   /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table;

   use ForwardsCallsTrait, HasEventsTrait;

  public function __call($methodName, $methodParameters)
  {
     return $this->forwardCallTo($this->newQuery(), $methodName, $methodParameters );
  }

  public static function __callStatic($methodName, $methodParameters)
  {

    return (new static)->$methodName(...$methodParameters);
  }

  /**
    * Convert the model to its string representation.
    *
    * @return string
    */
   public function __toString()
   {
       return $this->toJson();
   }

   public function setTable($table)
   {

   }

   public function getTable()
   {

   }

   public function save(array $options = [])
   {


   }

   public function newQuery()
   {
      return new QueryBuilder();
   }
 }
