<?php

namespace App\Vendor\Database\Support\Orm;

use App\Vendor\Database\Support\QueryBuilder as Builder;

class OrmBuilder
{

  /**
   * The base query builer instance
   *
   * @var App\Vendor\Database\Support\QueryBuilder
   */
   protected $query;

   public function __construct(Builder $query)
   {
     $this->query = $query;
   }
   


}
