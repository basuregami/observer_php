<?php

namespace App\Vendor\Database\Support\Traits;

trait HasEventsTrait
{

  protected $dispatchEvents = [];

  protected $observables = [];

  public function getObservableEvents()
  {
    return array_merge(
        [
          'create', 'created', 'saving', 'saved'
        ],
        $this->observables
      );
  }

  protected function fireModelEvent($event, $halt = true)
  {

  }

}
