<?php

  namespace App\Vendor\Environment\Adapter\Contract;

  interface AdapterInterface
  {

      /**
       * [Determines if the adapter is supported]
       * @return boolean [description]
       */
      public function isSupported();

      /**
       * Get as environment variable, if it exists;
       * @param  [string] $name [description]
       * @return [type]       [description]
       */
      public function get($name);

      /**
       * Set an environment variable
       *
       * @param [string] $name  [description]
       * @param [string|null] $value [description]
       */
      public function set($name, $value = null);

      /**
       * Clear an environment variable
       *
       * @param  [string] $name [description]
       * @return [void]       [description]
       */
      public function clear($name);

  }
