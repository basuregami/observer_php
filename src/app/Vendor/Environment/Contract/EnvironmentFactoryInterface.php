<?php
  namespace App\Vendor\Environment\Contract;

  interface EnvironmentFactoryInterface
  {
    public function create();

    public function createImmutable();
  }
