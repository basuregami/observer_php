<?php
 namespace Container\Contracts;

 interface ContainerInterface
 {
   /**
   * get the object from the container.
   * @params $id
   */
   public function get($id);

   /**
   * check if we have the object in the container.
   * @params $id
   */
   public function has($id);

 }
