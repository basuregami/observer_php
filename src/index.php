<?php

require_once __DIR__.'/bootstrap/bootstrap.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

use App\Models\User;
use App\Observer\BaseSubject;
use App\Observer\BaseObserver;
use App\Observer\UserObserver;
use App\Providers\ObserverServiceProvider;

// echo __DIR__.'./';

env('APP_NAME');


  // writeln('BEGIN TESTING OBSERVER PATTERN');
  // writeln('');


  // $provider = new ObserverServiceProvider();
  // $user = new User();
  // $userObserver = new UserObserver();
  // echo $user->create();
  // echo $provider->boot();
  //
  // echo "<pre>";

  //
  // $patternGossiper = new BaseSubject();
  // $patternGossipFan = new BaseObserver();
  // $patternGossiper->attach($patternGossipFan);
  // $patternGossiper->updateFavorites('abstract factory, decorator, visitor');
  // $patternGossiper->updateFavorites('abstract factory, observer, decorator');
  // $patternGossiper->detach($patternGossipFan);
  // $patternGossiper->updateFavorites('abstract factory, observer, paisley');

  $data = [
    'name' => 'basu regami',
    'age' => 26
  ];
  $response = User::create($data);
var_dump($response);
  // writeln('END TESTING OBSERVER PATTERN');
